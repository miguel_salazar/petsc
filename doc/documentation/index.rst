===================
PETSc Documentation
===================

.. toctree::
   :maxdepth: 1

   manual/index
   manualpages/index
   changes/index

* `TAO Users Manual <https://www.mcs.anl.gov/petsc/petsc-current/docs/tao_manual.pdf>`__
* `Function Index <../docs/manualpages/singleindex.html>`__
* `Examples Index <../docs/manualpages/help.html>`__
